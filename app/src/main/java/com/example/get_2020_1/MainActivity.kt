package com.example.get_2020_1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    lateinit var txtMain:TextView
    lateinit var etEmail:EditText
    lateinit var etPassword:EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.setContentView(R.layout.activity_main)

        txtMain=findViewById(R.id.txtMain)
        txtMain.text="Hello from UCJC"
        txtMain.textSize= 50F

        etEmail=findViewById(R.id.etEmail)
        etPassword=findViewById(R.id.etPassword)

        //var mapsActivityIntent:Intent=Intent(baseContext,MapsActivity)


    }

    fun clickBotonOK(v:View){
        //txtMain.textSize= 30F
        var textoEmail:String=etEmail.text.toString()
        var textoPassword:String=etPassword.text.toString()

        if (textoEmail=="pablodelatorreseoane@gmail.com" && textoPassword=="12345678") {
            txtMain.text = "BIENVENIDO!!"

            var intent:Intent=Intent(baseContext,MapsActivity::class.java)
            startActivity(intent)
            this.finish()

        }
        else{
            txtMain.text="ERROR EN USUARIO"
        }

        Log.v("MainActivity", "Your Email is: "+textoEmail+" Your Password is: "+textoPassword)

    }

    fun clickBotonCancel(v:View){


    }

}